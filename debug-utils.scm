(module debug-utils
    (breakpoint breakpoint-accessor breakpoint-proxy-vars
                instrument disabled-breakpoints clear-disabled-breakpoints
                breakpoint-run-state disable-all-breakpoints
                enable-all-breakpoints
                breakpoint-continuations breakpoint-continuation-vars)

(import chicken scheme srfi-1)
(use matchable box continuations)
(use-for-syntax srfi-1)
(use-for-syntax matchable)
(use-for-syntax continuations)

(define breakpoint-proxy-vars (make-parameter '()))
(define disabled-breakpoints (make-parameter '()))
(define (clear-disabled-breakpoints)
  (disabled-breakpoints '()))
(define breakpoint-run-state (make-parameter 'normal))
(define (disable-all-breakpoints)
  (breakpoint-run-state 'disabled))
(define (enable-all-breakpoints)
  (breakpoint-run-state 'normal))
(define breakpoint-continuations (make-parameter '()))
(define breakpoint-continuation-vars (make-parameter '()))

(define-syntax breakpoint-accessor
  (syntax-rules ()
    ((_ name)
     (define name
       (lambda a
         (match a
           ((var) (box-ref (alist-ref var (flatten (breakpoint-proxy-vars)))))
           ((var val) (box-set! (alist-ref var (flatten (breakpoint-proxy-vars))) val))
           (else (signal "1 or 2 arguments required"))))))))

;; (instrument
;;  (let ((x 10))
;;    (breakpoint "foo")
;;    (+ x 10)))

;; (match 'foo ('foo 'hi) (else 'boo))
;; (match '("baz" (x y) force)
;;   ((_ (_ ...) 'force)
;;    #t)
;;   (else #f))
;; (let ((force-run #t))
;;   (match 'baz
;;     ((or 'run-next 'force)
;;      #t)
;;     ((? (lambda (x) force-run))
;;      #t)
;;     (else #f)))
;; (match '("foo" 'no-force)
;;   ((_ (_ ...) ''no-force)
;;    #f)
;;   ((_ ''no-force)
;;    #f)
;;   (else 'baz))

(instrument
 (let ((x 10))
   (let loop ((i 0))
     (when (< i 5)
       (breakpoint "foo")
       (loop (+ i 1))))
   (breakpoint "bar")
   (+ x 1)))

(define-syntax breakpoint
  (ir-macro-transformer
   (lambda (x i c)
     (let ((id (->string (gensym)))
           (force-run
            (match (cdr x)
              ((_ (_ ...) (? (lambda (x) (equal? x (i ''no-force)) _)))
               #f)
              ((_ (? (lambda (x) (equal? x (i ''no-force)))))
               #f)
              ((_ (_ ...) (? (lambda (x) (equal? x (i ''force)) _)))
               #t)
              ;; if it only has a name it means it is a manually
              ;; inserted breakpoint and should be enabled by default.
              ((_) #t)
              (else #f))))
       `(begin
          ,@(match (cdr x)
              ((_ (_ ...) _)
               `((,(i 'breakpoint-proxy-vars)
                  (cons
                   (list
                    ,@(map (lambda (var)
                             `(cons
                               ',(i (i var)) .
                               ((make-box-variable ,(i (i (i (i var))))))))
                           (caddr x)))
                   ,(i '(breakpoint-proxy-vars))))))
              (else '()))
          (when
              (match (,(i 'breakpoint-run-state))
                (,(i ''run-next) #t)
                ((? (lambda (x)
                      (member (,(i 'quote) ,(i id)) (,(i 'disabled-breakpoints))
                              equal?)))
                 #f)
                ((? (lambda (x) ,force-run))
                 #t)
                (else #f))
            (let* ((cc (continuation))
                   (unique-id (->string (gensym))))
              (,(i 'breakpoint-continuations)
               (cons `(,unique-id . ,cc) (,(i 'breakpoint-continuations))))
              (,(i 'breakpoint-continuation-vars)
               (cons `(,unique-id . ,(list-copy (,(i 'breakpoint-proxy-vars))))
                     (,(i 'breakpoint-continuation-vars))))
              (print unique-id ":" ',id ":breakpoint for " ,(->string (cadr x)))
              (print (,(i 'breakpoint-proxy-vars)))
              (let loop ()
                (display "> ")
                (let* ((exp (read)))
                  (cond
                   ((equal? exp ,(i '',c))
                    (,(i 'breakpoint-run-state) ,(i ''run-next))
                    (void))
                   ((equal? exp ,(i '',run))
                    (,(i 'breakpoint-run-state) ,(i ''normal))
                    (void))
                   ((equal? exp ,(i '',disable))
                    (,(i 'disabled-breakpoints)
                     (cons ,id (,(i 'disabled-breakpoints)))
                     (print "breakpoint disabled")
                     (loop)))
                   ((equal? exp ,(i '',disable-breakpoint))
                    (,(i 'disabled-breakpoints)
                     (cons (->string (read)) (,(i 'disabled-breakpoints)))
                     (print "breakpoint disabled")
                     (loop)))
                   ((equal? exp ,(i '',go-to))
                    (let ((id (->string (read))))
                      (,(i 'breakpoint-proxy-vars)
                       (cdr (assoc id (,(i 'breakpoint-continuation-vars)))))
                      (throw (cdr (assoc id (,(i 'breakpoint-continuations))))
                             'nil)))
                   (else
                    (handle-exceptions
                        exn
                      (begin
                        (let* ((chain (with-output-to-string print-call-chain))
                               (cpa condition-property-accessor)
                               (exn-message (cpa 'exn 'message "(no message)"))
                               (exn-location
                                (cpa 'exn 'location "*ERROR LOCATION UNKNOWN*"))
                               (exn-arguments (cpa 'exn 'arguments '()))
                               (exn? (condition-predicate 'exn)))
                          (print (exn-location exn))
                          (print (exn-message exn))
                          (print (exn-arguments exn))
                          (print chain)
                          (loop)))
                      (unless (or (equal? exp ,(i '',disable-breakpoint))
                                  (equal? exp ,(i '',disable)))
                        (print
                         (eval
                          (,(i 'quasiquote)
                           (begin
                             (,(i 'unquote) exp)))))
                        (loop))))))))))))))

(define-syntax instrument
  (er-macro-transformer
   (lambda (x r c)
     (let ((tree
            (letrec
                ((insert-breakpoints
                  (match-lambda
                    ((let var ((binding values) ...) (x *** exp) ...)
                     `(let ,var ,(zip binding (map insert-breakpoints values))
                           (breakpoint
                            ,(->string `(let ,var ,(zip binding values) ,exp))
                            ,binding 'no-force)
                           (,(r 'let) ((,(r 'result)
                                        (,(r 'begin)
                                         ,@(map insert-breakpoints exp))))
                            (breakpoint-proxy-vars (cdr (breakpoint-proxy-vars)))
                            ,(r 'result))))
                    (((or ('let 'letrec) type) ((binding values) ...) (x *** exp) ...)
                     `(,type ,(zip binding (map insert-breakpoints values))
                             (breakpoint
                              ,(->string `(,type ,(zip binding values) ,exp))
                              ,binding 'no-force)
                             (,(r 'let) ((,(r 'result)
                                          (,(r 'begin)
                                           ,@(map insert-breakpoints exp))))
                              (breakpoint-proxy-vars (cdr (breakpoint-proxy-vars)))
                              ,(r 'result))))
                    ((or (x ()) (x #f) (x #t) (? string? x) (? number? x) (? char? x))
                     x)
                    ((xpr ...)
                     `(begin (breakpoint ,(->string xpr) 'no-force)
                             ,(map insert-breakpoints xpr)))
                    (xpr xpr))))
              (insert-breakpoints (cadr x)))))
       `(,(r 'begin)
         (,(r 'let)
          ((,(r 'r)
            ,tree))
          (breakpoint-proxy-vars '())
          ,(r 'r)))))))

)
